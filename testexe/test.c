#ifdef _WIN32
#include<Windows.h>
#else
#include<stdio.h>
#endif

#include<process.h>
#include<stdio.h>

int main(int argc, char ** argv) {
    int *a = (int*)malloc(sizeof(int));
    *a = 4;
    while (1) {
        printf("a == %d at %p with pid %d, size of an int on this machine is %I64d\n", *a, a, getpid(), sizeof(int));
        Sleep(3000);
    }
    return 1;
}