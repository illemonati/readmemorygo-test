package main

import (
	"bytes"
	"encoding/binary"
	"fmt"
	"github.com/JamesHovious/w32"
)

func main() {
	baseAddr := 0x162490
	pid := 14188
	process, err := w32.OpenProcess(w32.PROCESS_VM_OPERATION|w32.PROCESS_VM_READ|w32.PROCESS_VM_WRITE, false, uint32(pid))
	if err != nil {
		fmt.Println(err)
		return
	}
	mem, err := w32.ReadProcessMemory(process, uint32(baseAddr), 4)
	a := int32(0)
	// little endian
	for i, _ := range mem {
		a = (a << 8) | int32(mem[len(mem)-1-i])
	}
	fmt.Println(a)

	newNumber := int32(50)
	newBuf := new(bytes.Buffer)
	err = binary.Write(newBuf, binary.LittleEndian, newNumber)

	err = w32.WriteProcessMemory(process, uint32(baseAddr), newBuf.Bytes(), 4)
}
